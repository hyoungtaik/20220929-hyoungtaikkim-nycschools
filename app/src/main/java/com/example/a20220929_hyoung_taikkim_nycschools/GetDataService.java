package com.example.a20220929_hyoung_taikkim_nycschools;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface GetDataService {
    @GET("/resource/s3k6-pzi2.json")
    Call<List<SchoolNameData>> getAllSchoolNames();

    @GET("/resource/f9bf-2cp4.json")
    Call<List<SatData>> getAllSats();
}
