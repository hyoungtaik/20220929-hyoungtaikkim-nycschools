package com.example.a20220929_hyoung_taikkim_nycschools;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.util.HashMap;
import java.util.List;

/** main comment by Hyoung-Taik Kim on Sep. 29, 2022 :
 *     While I am working full time even till late night often,
 *         working on the assignment has been somewhat challenging.
 *         So I just tried to make it work, while I spend as little time as possible at the moment.
 *
 * If I had more time, I would do
 *      1. improve code quality including better structure, comment, class name, method name and etc.
 *      3. I would write down more thorough unit tests. (At the moment, I have shown little unit tests mainly for demo purpose.
 *      4. Better look and feel.
 *      5. Better take care of any possible edge case.
 *
 */
public class MainActivity extends AppCompatActivity implements SchoolNamesAdapter.ItemClickListener {

    public static String TAG = MainActivity.class.getSimpleName();

    private SchoolNamesAdapter mAdapter;
    private RecyclerView mRecyclerView;

    private MainViewModel mMainViewModel;

    private HashMap<String, SatData> mMap = new HashMap<>(); // key will be dbn and value will be SatData. using map for fast fetching data.

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mMainViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        mMainViewModel.init();

        addObserver();
    }

    public void addObserver() {
        mMainViewModel.getSchoolNamesLiveData().observe(this, new Observer<List<SchoolNameData>>() {
            @Override
            public void onChanged(List<SchoolNameData> schoolNamesResponse) {
                if ((schoolNamesResponse != null) && (!schoolNamesResponse.isEmpty())) {
                    generateSchoolDataList(schoolNamesResponse);
                }
            }
        });

        mMainViewModel.getSatsMapLiveData().observe(this, new Observer<HashMap<String, SatData>>() {
            @Override
            public void onChanged(HashMap<String, SatData> satsResponse) {
                if ((satsResponse != null) && (!satsResponse.isEmpty())) {
                    mMap = satsResponse;
                    Toast.makeText(getApplicationContext(), "Sat Data is now ready. Click any school!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void generateSchoolDataList(List<SchoolNameData> schoolNameList) {
        mRecyclerView = findViewById(R.id.rvSchoolNames);
        mAdapter = new SchoolNamesAdapter(this, schoolNameList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MainActivity.this);
        mRecyclerView.setLayoutManager(layoutManager);
        mAdapter.setClickListener(this);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onSchoolItemClick(View view, int position) {
        if ((mMap == null) || (mMap.size() == 0) || (mAdapter == null)) {
            Toast.makeText(this, "SAT information might not be ready yet. Try it little later ", Toast.LENGTH_LONG).show();
            return;
        }

        try {
            String myDbn = mAdapter.getItem(position).getDbn();
            String myReading = mMap.get(myDbn).getSat_critical_reading_avg_score();
            String myMath = mMap.get(myDbn).getSat_math_avg_score();
            String myWriting = mMap.get(myDbn).getSat_writing_avg_score();

            Toast.makeText(this, "For " + mAdapter.getItem(position).getSchool_Name() + ", sat_critical_reading_avg_score:  "
                    + myReading + ", sat_math_avg_score: " + myMath
                    + ", sat_writing_avg_score: " + myWriting, Toast.LENGTH_LONG).show();
        } catch (Exception e1) {
            Log.e(TAG, "exception at onSchoolItemClick. e1 = " + e1.toString());
            Toast.makeText(this, "For " + mAdapter.getItem(position).getSchool_Name() + " sat data is unavailable.", Toast.LENGTH_LONG).show();
        }
    }

}