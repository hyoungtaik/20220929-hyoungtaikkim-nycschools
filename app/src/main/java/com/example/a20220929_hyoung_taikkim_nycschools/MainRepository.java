package com.example.a20220929_hyoung_taikkim_nycschools;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainRepository {

    public static String TAG = MainRepository.class.getSimpleName();

    private MutableLiveData<List<SchoolNameData>> schoolNamesLiveData;
    private MutableLiveData<HashMap<String, SatData>> satsMapLiveData;


    public MutableLiveData<List<SchoolNameData>> getSchoolData() {
        MutableLiveData<List<SchoolNameData>> myMutableList = new MutableLiveData<>();

        /*Create handle for the RetrofitInstance interface*/
        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<List<SchoolNameData>> call = service.getAllSchoolNames();
        call.enqueue(new Callback<List<SchoolNameData>>() {
            @Override
            public void onResponse(Call<List<SchoolNameData>> call, Response<List<SchoolNameData>> response) {

                if (response != null) {
                    if (response.isSuccessful()) {
                        List<SchoolNameData> myList = response.body();

                        // sort it by school name
                        Collections.sort(myList, new Comparator<SchoolNameData>() {
                            @Override
                            public int compare(SchoolNameData t1, SchoolNameData t2) {
                                return t1.getSchool_Name().compareTo(t2.getSchool_Name());
                            }
                        });

                        myMutableList.setValue(myList);
                    } else {
                        Log.e(TAG, "response is not successful");
                    }
                } else {
                    Log.e(TAG, "response is null");
                }
            }

            @Override
            public void onFailure(Call<List<SchoolNameData>> call, Throwable t) {
                Log.e(TAG, "onFailure for fetching school name data");
            }
        });

        return myMutableList;
    }

    public MutableLiveData<HashMap<String, SatData>> getSatData() {
        MutableLiveData<HashMap<String, SatData>> myMutableMap = new MutableLiveData<>();

        /*Create handle for the RetrofitInstance interface*/
        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<List<SatData>> call = service.getAllSats();
        call.enqueue(new Callback<List<SatData>>() {
            @Override
            public void onResponse(Call<List<SatData>> call, Response<List<SatData>> response) {

                if (response != null) {
                    if (response.isSuccessful()) {
                        List<SatData> myList = response.body();
                        HashMap<String, SatData> myMap = buildHashMap(myList);
                        myMutableMap.setValue(myMap);
                    } else {
                        Log.e(TAG, "response is not successful");
                    }
                } else {
                    Log.e(TAG, "response is null");
                }
            }

            @Override
            public void onFailure(Call<List<SatData>> call, Throwable t) {
                Log.e(TAG, "onFailure for fetching sat data");
            }
        });
        return myMutableMap;
    }

    /**
     * I just try to use hashmap for fast fetching the data I want.
     * @param inList list of SatData
     */
    private HashMap<String, SatData> buildHashMap(List<SatData> inList) {
        HashMap<String, SatData> myMap = new HashMap<>(); // key will be dbn and value will be SatData. using map for fast fetching data.

        if ((inList == null) || (inList.isEmpty()))
            return myMap;

        for (int i = 0; i < inList.size(); i++) {
            myMap.put(inList.get(i).getDbn(), inList.get(i));
        }

        return myMap;
    }
}
