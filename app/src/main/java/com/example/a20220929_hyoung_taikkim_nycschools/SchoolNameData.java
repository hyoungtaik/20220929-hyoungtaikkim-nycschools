package com.example.a20220929_hyoung_taikkim_nycschools;

import com.google.gson.annotations.SerializedName;

public class SchoolNameData {

    @SerializedName("dbn")
    private String dbn;

    @SerializedName("school_name")
    private String school_name;

    public SchoolNameData(String inDbn, String inSchoolName) {
        dbn = inDbn;
        school_name = inSchoolName;
    }

    public String getDbn() {
        return dbn;
    }

    public String getSchool_Name() {
        return school_name;
    }

    public void setDbn(String inDbn) {
        dbn = inDbn;
    }

    public void setSchool_name(String inName) {
        school_name = inName;
    }
}
