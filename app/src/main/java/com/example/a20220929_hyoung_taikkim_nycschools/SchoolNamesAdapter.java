package com.example.a20220929_hyoung_taikkim_nycschools;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class SchoolNamesAdapter extends RecyclerView.Adapter<SchoolNamesAdapter.ViewHolder> {

    private List<SchoolNameData> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;

    SchoolNamesAdapter(Context context, List<SchoolNameData> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.school_name_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String schoolName = mData.get(position).getSchool_Name();
        holder.myTextView.setText(schoolName);
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView myTextView;

        ViewHolder(View itemView) {
            super(itemView);
            myTextView = itemView.findViewById(R.id.tvSchoolName);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onSchoolItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    public SchoolNameData getItem(int id) {
        return mData.get(id);
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onSchoolItemClick(View view, int position);
    }
}
