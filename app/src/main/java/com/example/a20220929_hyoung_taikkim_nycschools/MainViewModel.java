package com.example.a20220929_hyoung_taikkim_nycschools;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.HashMap;
import java.util.List;

public class MainViewModel extends AndroidViewModel {
    private MutableLiveData<List<SchoolNameData>> schoolNamesLiveData;
    private MutableLiveData<HashMap<String, SatData>> satsMapLiveData;

    public MainViewModel(@NonNull Application application) {
        super(application);
    }

    public void init() {
       MainRepository myRepo = new MainRepository();
       schoolNamesLiveData = myRepo.getSchoolData();
       satsMapLiveData = myRepo.getSatData();
    }

    public LiveData<List<SchoolNameData>> getSchoolNamesLiveData() {
        return schoolNamesLiveData;
    }

    public LiveData<HashMap<String, SatData>> getSatsMapLiveData() {
        return satsMapLiveData;
    }
}
