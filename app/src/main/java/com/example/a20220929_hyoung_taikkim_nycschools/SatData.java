package com.example.a20220929_hyoung_taikkim_nycschools;

import com.google.gson.annotations.SerializedName;

public class SatData {

    @SerializedName("dbn")
    private String dbn;

    @SerializedName("school_name")
    private String school_name;

    @SerializedName("sat_critical_reading_avg_score")
    private String sat_critical_reading_avg_score;

    @SerializedName("sat_math_avg_score")
    private String sat_math_avg_score;

    @SerializedName("sat_writing_avg_score")
    private String sat_writing_avg_score;

    public String getDbn() {
        return dbn;
    }

    public String getSchool_Name() {
        return school_name;
    }

    public void setDbn(String inDbn) {
        dbn = inDbn;
    }

    public void setSchool_name(String inName) {
        school_name = inName;
    }

    public String getSat_critical_reading_avg_score() {
        return sat_critical_reading_avg_score;
    }

    public String getSat_math_avg_score() {
        return sat_math_avg_score;
    }

    public String getSat_writing_avg_score() {
        return sat_writing_avg_score;
    }

    public void settSat_critical_reading_avg_score(String inScore) {
        sat_critical_reading_avg_score = inScore;
    }

    public void setSat_math_avg_score(String inScore) {
        sat_math_avg_score = inScore;
    }

    public void setSat_writing_avg_score(String inScore) {
        sat_writing_avg_score = inScore;
    }


}
