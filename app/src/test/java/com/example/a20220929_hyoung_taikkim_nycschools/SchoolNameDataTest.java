package com.example.a20220929_hyoung_taikkim_nycschools;

import android.content.Context;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import static androidx.test.core.app.ApplicationProvider.getApplicationContext;

@RunWith(RobolectricTestRunner.class)
public class SchoolNameDataTest {
    private Context mContext = null;

    @Before
    public void setup() {
        mContext = getApplicationContext();
    }


    @Test
    public void getDbnTest1() {
        SchoolNameData myData = new SchoolNameData("dbn1", "mySchool");
        Assert.assertEquals("dbn1", myData.getDbn());
    }

    @Test
    public void getDbnTest2() {
        SchoolNameData myData = new SchoolNameData("", "mySchool");
        Assert.assertEquals("", myData.getDbn());
    }

    @Test
    public void getDbnTest3() {
        SchoolNameData myData = new SchoolNameData(null, "mySchool");
        Assert.assertNull(myData.getDbn());
    }

    @Test
    public void geSchool_NameTest1() {
        SchoolNameData myData = new SchoolNameData("dbn1", "mySchool");
        Assert.assertEquals("mySchool", myData.getSchool_Name());
    }

    @Test
    public void geSchool_NameTest2() {
        SchoolNameData myData = new SchoolNameData("dbn1", "");
        Assert.assertEquals("", myData.getSchool_Name());
    }

    @Test
    public void geSchool_NameTest3() {
        SchoolNameData myData = new SchoolNameData("dbn1", null);
        Assert.assertNull(myData.getSchool_Name());
    }
}
