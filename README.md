# 20220929-Hyoung-TaikKim-NYCSchools

comment by Hyoung-Taik Kim on Sep. 29, 2022 at hyoungtaik@yahoo.com :
      While I am working full time even till late night often,
          working on the assignment has been somewhat challenging.
          So I just tried to make it work, while I spend as little time as possible at the moment.
 
      If I had more time, I would do
       1. improve code quality including better structure, comment, class name, method name and etc.
       2. I would write down more thorough unit tests. (At the moment, I have shown little unit tests mainly for demo purpose.
       2. Better look and feel.
       4. Better take care of any possible edge case.

